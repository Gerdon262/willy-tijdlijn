module.exports = {
  verbose: false,
  moduleFileExtensions: ['js', 'json', 'vue'],
  globals: {
    NODE_ENV: 'test'
  },
  testPathIgnorePatterns: ['.eslintrc.js', '.git'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.vue$': 'vue-jest'
  },
  snapshotSerializers: ['jest-serializer-vue'],
  collectCoverageFrom: ['vue/**/*.{vue,js}'],
  coverageReporters: ['clover', 'lcov', 'text']
}
