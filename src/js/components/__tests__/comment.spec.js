import { shallowMount } from "@vue/test-utils";
import Comment from "../Comment.vue";
import comment from "../__fixtures__/comment";

function setupComponent() {
  return new shallowMount(Comment, {
    propsData: {
      comment
    }
  });
}

describe("Comment", () => {
  describe("render", () => {
    it("shows the correct information", () => {
      const wrapper = setupComponent();
      expect(wrapper.find(".comment__body").text()).toBe(comment.body);
    });
  });
  describe("emit", () => {
    it("emits delete to the parent", () => {
      const wrapper = setupComponent();
      wrapper.find(".comment__remove").trigger("click");
      expect(wrapper.emitted("delete")).toBeTruthy();
      expect(wrapper.emitted("delete")[0]).toEqual([comment.id]);
    });
  });
});
