import faker from "faker";
import { shallowMount } from "@vue/test-utils";
import Header from "../Header.vue";

describe("Comment", () => {
  describe("render", () => {
    it("shows the correct information", () => {
      const title = faker.name.firstName();
      const wrapper = new shallowMount(Header, {
        propsData: {
          title
        }
      });
      expect(wrapper.find(".header__title").text()).toBe(title);
    });
  });
});
