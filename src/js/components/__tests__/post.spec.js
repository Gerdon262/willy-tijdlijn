import faker from 'faker'
import { getFormattedDate } from '../../helpers/dateFormat'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Post from '../Post.vue'
import Comment from '../Comment.vue'
import post from '../__fixtures__/post'
import comment from '../__fixtures__/comment'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

function setupComponent() {
  // Mock the store
  const getters = {
    getCommentsForPost: () => jest.fn().mockReturnValueOnce([comment])
  }
  const store = new Vuex.Store({
    modules: {
      comments: {
        namespaced: true,
        getters
      }
    }
  })
  return new shallowMount(Post, {
    store,
    localVue,
    propsData: {
      post
    }
  })
}

describe('Post', () => {
  beforeEach(() => {})
  describe('render', () => {
    it('shows the correct information', () => {
      const wrapper = setupComponent()
      expect(wrapper.find('.header__title').text()).toBe(post.title)
      const toBeDate = getFormattedDate(post.added)
      expect(wrapper.find('.header__date').text()).toBe(toBeDate)
      expect(wrapper.find('.post__body').text()).toBe(post.body)
      expect(wrapper.contains('.comments__title')).toBeTruthy()
      expect(wrapper.contains(Comment)).toBeTruthy()
    })
  })
  describe('emit', () => {
    it('emits sendComment', () => {
      const wrapper = setupComponent()
      const body = faker.lorem.sentence()
      const input = wrapper.find('.comment__input')
      input.setValue(body)
      input.trigger('keydown.enter')
      expect(wrapper.emitted('comment')).toBeTruthy()
      expect(wrapper.emitted('comment')[0]).toEqual([
        {
          postId: post.id,
          body
        }
      ])
      expect(input.text()).toBe('')
    })
    it('emits deleteComment', () => {
      const wrapper = setupComponent()
      wrapper.vm.deleteComment(comment.id)
      expect(wrapper.emitted('deleteComment')).toBeTruthy()
      expect(wrapper.emitted('deleteComment')[0]).toEqual([comment.id])
    })
  })
})
