import dayjs from "dayjs";

export const getFormattedDate = date => {
  return dayjs(new Date(date)).format("dddd DD MMMM YYYY");
};

export const getSimpleDate = () => {
  return dayjs(new Date()).format("YYYY-MM-DD");
};
