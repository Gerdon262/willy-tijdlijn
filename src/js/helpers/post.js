import VueTypes from "vue-types";

export const postShape = VueTypes.shape({
  id: VueTypes.number.isRequired,
  title: VueTypes.string.isRequired,
  body: VueTypes.string.isRequired,
  added: VueTypes.string.isRequired
});
