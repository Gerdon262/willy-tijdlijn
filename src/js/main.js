import Vue from "vue";
import App from "./templates/App.vue";
import store from "./store/store";

import Message from "element-ui/lib/message";
Vue.prototype.$message = Message;

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
