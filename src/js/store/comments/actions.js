export default {
  setComments({ commit }, comments) {
    commit("SET_COMMENTS", comments);
  },
  addComment({ commit }, comment) {
    commit("ADD_COMMENT", comment);
  },
  deleteComment({ commit }, commentId) {
    commit("DELETE_COMMENT", commentId);
  }
};
