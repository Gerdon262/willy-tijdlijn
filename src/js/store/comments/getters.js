export default {
  getCommentsForPost: state => postId => {
    const comments = [];
    state.comments.forEach(comment => {
      if (comment.postId === postId) {
        comments.push(comment);
      }
    });
    return comments;
  }
};
