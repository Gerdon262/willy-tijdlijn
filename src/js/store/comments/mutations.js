export default {
  SET_COMMENTS(state, comments) {
    state.comments = comments
  },
  ADD_COMMENT(state, comment) {
    state.comments.unshift(comment)
  },
  DELETE_COMMENT(state, commentId) {
    state.comments.splice(state.comments.findIndex(comment => comment.id === commentId), 1)
  }
}
