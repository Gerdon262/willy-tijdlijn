export default {
  setPosts({ commit }, posts) {
    commit('SET_POSTS', posts)
  },
  addPost({ commit }, post) {
    commit('ADD_POST', post)
  }
}
